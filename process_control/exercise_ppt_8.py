from multiprocessing import Process, Queue, Pipe
from time import sleep


def worker(pipe: Pipe):
    print('Awake!')
    number = pipe.recv()
    print("computing...")
    sleep(1)
    pipe.send(number ** 2)


def main():
    print('Main - Starting')
    conn1, conn2 = Pipe()
    p = Process(target=worker, args=(conn1,))
    p.start()
    sleep(1)
    conn2.send(3)
    print('Main - waiting for data')
    print(conn2.recv())
    print('Main - Done')


if __name__ == '__main__':
    main()
