from multiprocessing import Semaphore
from threading import Thread, Lock, currentThread
from time import sleep


def worker(semafore):
    with semafore:
        print(currentThread().getName() + " - entered")
        sleep(0.5)
        print(currentThread().getName() + " - exiting")


print('MainThread - Starting')

semafore = Semaphore(2)
for i in range(0, 5):
    thread = Thread(name='T' + str(i), target=worker, args=[semafore])
    thread.start()


print('MainThread - Done')

