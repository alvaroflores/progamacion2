import collections


def f(accum=0, m=10):
    for i in range(m):
        rcv = yield accum
        if rcv is None:
            accum -= 1
        else:
            accum = rcv


g = f(10, 8)
print(next(g))
print(next(g))
print(next(g))
print(g.send(10))
print(next(g))
print(next(g))
print(next(g))


def worker(f):
    tasks = collections.deque()
    value = None
    while True:
        batch = yield value
        value = None
        if batch is not None:
            tasks.extend(batch)
        else:
            if tasks:
                args = tasks.popleft()
                value = f(args)


def example_worker():
    w = worker(str)
    w.send(None)
    w.send([(1,), (2,), (3,)])
    w.throw(ValueError)
    w.close()