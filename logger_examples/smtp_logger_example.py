import logging.config

logging.config.fileConfig('logger.conf')

# create logger
logger = logging.getLogger('simpleExample')

# 'application' code
logging.debug('debug message')
logging.info('info message')
logging.warning('warn message')
logging.error('error message')
logging.critical('critical message')
