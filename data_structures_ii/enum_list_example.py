from dataclasses import dataclass
from collections import deque
import heapq
import datetime as dt
from enum import Enum, auto


class Subject(Enum):
    """Course Subjects."""
    ALGO = "ALGO"
    PROG_I = auto()
    PROG_II = auto()


@dataclass(order=True)
class Assignment:
    """Stores information related with a student assignment."""
    due_date: dt.date
    subject: Subject
    description: str

    def __lt__(self, other: 'Assignment') -> bool:
        """Priority set based on due date (earliest Assignment has higher priorioty)."""
        return self.due_date < other.due_date


# Implementing a stack, LIFO.
dq = []
dq.append(Assignment(dt.date(2011, 1, 1), Subject.ALGO, "Assignment number 01"))
dq.append(Assignment(dt.date(2011, 2, 1), Subject.PROG_II, "Assignment number 03"))
assignment = dq.pop()
print(vars(assignment))
print(f'Stack, last out: {assignment}')


# Implementing a queue, with a doble ended queue.
dq = deque([])
dq.append(Assignment(dt.date(2011, 1, 1), Subject.ALGO, "Assignment number 01"))
dq.append(Assignment(dt.date(2011, 2, 1), Subject.PROG_II, "Assignment number 03"))
assignment = dq.popleft()
dq.appendleft(Assignment(dt.date(2023, 2, 1), Subject.PROG_I, "Assignment number 02"))
print(f'Stack, first out: {assignment}')
print(f'Stack, first out: {dq.popleft()}')

# Implementing a priority queue with a heap.
hq = []
heapq.heapify(hq)
heapq.heappush(hq, Assignment(dt.date(2011, 1, 1), Subject.ALGO, "Assignment number 01"))
heapq.heappush(hq, Assignment(dt.date(2011, 2, 1), Subject.ALGO, "Assignment number 01"))

assignment = heapq.heappop(hq)
print(f'Heap, earliest first: {assignment}')
heapq.heappush(hq, Assignment(dt.date(2010, 1, 1), Subject.PROG_I, "Assignment number 01"))

assignment = heapq.heappop(hq)
print(f'Heap, next: {assignment}')

hq = []
heapq.heapify(hq)
heapq.heappush(hq, 1)
heapq.heappush(hq, 3)

assignment = heapq.heappop(hq)
print(f'Heap, earliest first: {assignment}')
heapq.heappush(hq, 2)
heapq.heappush(hq, 2)

assignment = heapq.heappop(hq)
print(f'Heap, next: {assignment}')

