from collections import namedtuple
import numpy as np
Statistics = namedtuple('Statistics', ['media', 'std', 'max', 'min'])


def statistics(v: np.ndarray) -> Statistics:
    return Statistics(media=v.mean(), std=v.std(), max=max(v), min=min(v))


r = statistics(np.array([1, 2, 3, 4, 5]))

print(r)