import time
from collections import OrderedDict

# Note, built-in dict remembers order.

a = dict(john='A', mary='A+')

# https://docs.python.org/3/library/stdtypes.html#typesmapping
# Remove and return a (key, value) pair from the dictionary. Pairs are returned in LIFO order.

print(a.popitem())

# Return an iterator over the keys, values or items (represented as tuples of (key, value))
# in the dictionary.
# Keys and values are iterated over in insertion order.

a = dict(john='A', mary='A+')
it = iter(a.keys())
print(next(it))

# The objects returned by dict.keys(), dict.values() and dict.items() are view objects.
# They provide a dynamic view on the dictionary’s entries, which means that when the
# dictionary changes, the view reflects these changes.

a = dict(john='A', mary='A+')

# Return a reverse iterator over the keys, values or items of the dictionary.
# The view will be iterated in reverse order of the insertion.
list(reversed(a.keys()))

# Now, lets see examples of OrderedDict


class LastUpdatedOrderedDict(OrderedDict):
    """Store items in the order the keys were last added"""

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        self.move_to_end(key)


class TimeBoundedLRU:
    """LRU Cache that invalidates and refreshes old entries."""

    def __init__(self, func, maxsize=128, maxage=30):
        self.cache = OrderedDict()      # { args : (timestamp, result)}
        self.func = func
        self.maxsize = maxsize
        self.maxage = maxage

    def __call__(self, *args):
        if args in self.cache:
            self.cache.move_to_end(args)
            timestamp, result = self.cache[args]
            if time.time() - timestamp <= self.maxage:
                return result
        result = self.func(*args)
        self.cache[args] = time.time(), result
        if len(self.cache) > self.maxsize:
            self.cache.popitem(False)
        return result
