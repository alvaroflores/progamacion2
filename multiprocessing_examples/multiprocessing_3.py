import multiprocessing as mp
import time

start = time.perf_counter()


def do_something(seconds):
    print(f'Sleeping {seconds} second(s)...')
    time.sleep(seconds)
    print(f'Done Sleeping...{seconds}')


if __name__ == '__main__':
    mp.set_start_method('fork')

    processes = []
    for _ in range(10):
        p = mp.Process(target=do_something, args=(1,))
        p.start()
        processes.append(p)

    for process in processes:
        process.join()

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start, 2)} second(s)')