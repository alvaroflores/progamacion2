from flask import Flask, jsonify, request
import logging

LOG_FORMAT = "%(asctime)s [%(levelname)s] %(module)s: %(funcName)s: %(message)s"

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
formatter = logging.Formatter(LOG_FORMAT)
ch.setFormatter(formatter)

PRICES = {
    "70": "160",
    "60": "140",
    "50": "100"
}


def create_service():
    new_app = Flask(__name__)

    @new_app.route('/', methods=['GET'])
    def dynamic_price():
        rooms = request.args.get('rooms')
        price = PRICES.get(rooms)

        return jsonify({'price': price})

    new_app.logger.addHandler(ch)
    new_app.logger.setLevel(logging.DEBUG)

    return new_app


if __name__ == '__main__':
    app = create_service()
    app.run(debug=True, port=8080)
