class Evens(object):

    def __init__(self, limit):
        self.limit = limit
        self.val = 0

    # Makes this class iterable
    def __iter__(self):
        return self

    # Makes this class an iterator
    def __next__(self):
        if self.val > self.limit:
            raise StopIteration
        else:
            return_val = self.val
            self.val += 2
            return return_val


it = Evens(4)

print(next(it))
print(next(it))
print(next(it))

try:
    print(next(it))
except StopIteration:
    print('end')
