def f(accum=0):
    while True:
        rcv = yield accum
        if rcv is None:
            accum -= 1
        else:
            accum = rcv


g = f()
print(next(g))
print(next(g))
print(next(g))
print(g.send(10))
print(next(g))
print(next(g))
print(next(g))
