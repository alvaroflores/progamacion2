import time
from functools import lru_cache

@lru_cache(maxsize=128)
def fib_without_cache(n):
    if n < 2:
        return n
    return fib_without_cache(n - 1) + fib_without_cache(n - 2)


start_time = time.time()
print(fib_without_cache(33))
print(f"---{time.time() - start_time} seconds ---")