# Arrays

import numpy as np

a = np.array([1, 2, 3])   # Create a rank 1 array
print(type(a))            # Prints "<class 'numpy.ndarray'>"
print(a.shape)            # Prints "(3,)"
print(a[0], a[1], a[2])   # Prints "1 2 3"
a[0] = 5                  # Change an element of the array
print(a)                  # Prints "[5, 2, 3]"

b = np.array([[1,2,3],[4,5,6]])    # Create a rank 2 array
print(b.shape)                     # Prints "(2, 3)"
print(b[0, 0], b[0, 1], b[1, 0])   # Prints "1 2 4"

import time
import sys

a = np.array([1,2,3,4])
print(a)
print(a.shape) # dimensions
print(a.dtype) # datatypes.
print(a.ndim) # number of dimensions
print(a.size) # total number of elements
print(a.itemsize) # size in bytes of each element int64 -> 8 bytes

print(a[1])
a[0] = 10
print(a)

b = a * np.array([4,3,2,1])
# print(b)

# difference between python list and np.array
l = [1,2,3]
a = np.array([1,2,3])
l.append(4)
# l = l + [4]
# a.append(4)
a = a + np.array([4])  # Mathematical operation for each element in a. NOT increasing the size of the list. Broadcasting
# print(l)
# print(a)

# numpy dot
a1 = np.array([1,2,3])
a2 = np.array([4,5,6])
dot = np.dot(a1, a2)
print(dot)
mul1 = a1 * a2
dot = mul1.sum()
print(dot)
multidimentional = np.array([1,2], [1,2,3])
print(multidimentional)
