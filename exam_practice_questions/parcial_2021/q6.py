from math import pi, sqrt


class Circle:
    def __init__(self):
        self._r = None

    @property
    def radius(self):
        return self._r

    @radius.setter
    def radius(self, r):
        if r > 0:
            self._r = r

    @property
    def perimeter(self):
        return 2 * pi * self._r

    @perimeter.setter
    def perimeter(self, p):
        self._r = p / 2 / pi

    @property
    def area(self):
        return pi * self._r ** 2

    @area.setter
    def area(self, a):
        self._r = sqrt(a / pi)


c = Circle()
c.perimeter = 2 * pi
print(c.perimeter)  # returns 2 * pi, 6.283185
print(c.radius)  # returns 1.0

c = Circle()
c.area = 4 * pi
print(c.area)  # returns 4 * pi, 12.566
print(c.radius)  # returns 2.0

grades_1 = [9, 5, 7]
grades_2 = [9, 5, 7]
print(id(grades_1) == id(grades_2))

