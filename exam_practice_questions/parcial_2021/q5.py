from math import log


def log_transform(func: callable) -> callable:
    def g(x: float) -> float:
        return func(log(x, 2))
    return g


@log_transform
def f(x):
    return x ** 2


f(2)  # 1.0
f(4)  # 4.0
f(8)  # 9.0


def absolute(func: callable) -> callable:
    def g(x: float) -> float:
        return func(abs(x))
    return g


@absolute
def f(x):
    return x ** 3


f(2)  # 8.0
f(-2)  # 8.0
