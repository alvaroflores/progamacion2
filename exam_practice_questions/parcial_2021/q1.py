# This call does not fail because z is defined at global namespace
# z is resolved first looking at local namespace, then global and then
# resolved there

def f(x):
    global z
    z = 4
    print(x)
    print(z)


z = 3
print(z)
f(3)
print(z)

"""
F: z cannot be resolved when f is called
T: When the instruction print(x) is executed, there are two levels in the execution stack
F: z is defined in the local namespace of f
F: We can modify the global value of z in f by assigning it a new value inside f's code block
"""