"""heapq"""
# Crea un programa que encuentre el elemento N ( 1 <= N <= array_length)
# más largo de una lista desordenada utilizando heap.

# Utilizando heap, crea una funcion que encuentre los 3 numeros más grandes de una lista.


# Escribir un programa que ejecute el producto de los 3 numeros más grandes de una lista.
from itertools import accumulate

"""Generadores"""

# Crear un generador que recorra de forma indefinida una cadena de texto, caracter a caracter, hasta que se introduzca
# en el generador un elemento None, en cuyo caso terminará.

# Crear un generador que me devuelva números aleatorios y siempre que se vuelva a ejecutar, me devuelva la diferencia
# de tiempo desde la ultima ejecución del genrador. (1, 0) , (8, 1.24352). Si la diferencia de tiempo es mayor que 5 segundos
# deberá lanzar una excepcion y terminar el probrama


"""High order functions"""

# Utilizando higher order function de forma manual, dadao una lista, devuelva la misma con la función aplicada.
# Crear esto para que devuelva todos los elemenos en mayuscula, para que devuelva la version en string the una lista de numeros,
# y para que capitalice una lista de strings. Son 3 llamadas a la misma funcion.


#  Utilizando map, escribe un programa que multiplique los números de una lista

#  Crear un programa que sume tres listas utilizando map y lambda.


#  Crear un programa que convierta los objetos de una lista a mayuscula y minuscula y eliminar los duplicados.
# Utilizar map.


"""Logging info"""

#  Create a logger configuration programmatically that have two different handlers, one will be a handler for writing
#    error or exeption messages to the file and the other handler will be to print in the console messages, allowing
#    all type of levels for this one.
#    For the file handler we will use a complex formatter like:
#          - [ %(2022-10-04) - %(LOGGERS_NAME) - %(INFO) - %(current_file)s:%(49)d ] %(This is the log message)s
#   The stream handler will use a different logging formatter like:
#          - %(INFO)s - %(LOGGERS_NAME)s %(asctime)s - %(message)s
#   Note: you will need to replace the variables names inside %() for this to work as expected.

"""Classes"""
#  Create an Alumno class and prevent the modification of the attributes inside it.
#  Do not use dataclass for this exercise.

# Crear una lista de clases PEDIDO que a parte de otras variables, puede tener una variable tipo float que marca
# el importe del pedido, puede venir definida de cualquier manera. Quiero poder crear una lista en la que los indices
# de la lista creada marquen el numero de decimales maximos que tiene que tener dicho pedido. Si esta el pedido1 en el
# indice 1 se tendra que redondear el importe a 1 solo decimal. Si estuviera en el indice 5, tendriamos que redondearlo
# a 5 decimales.


# Crear una clase Vehiculo, Motocicleta y Coche.
# Instancia una clase u otra en funcion de las ruedas que tengas de inicio, si tienes 4 patas será un coche,
# si tienes dos será una Motocicleta. PAra este ejercicio es importante el uso de herencia y polimorfismo.
# Cualquier instancia de vehiculo puede arrancar, todos tienen un numero de ruedas, una potencia, una velocidad maxima
# y una aceleración.



