import time
import threading


def worker():
    for i in range(0, 10):
        print(i, end=' ', flush=True)
        time.sleep(1)


if __name__ == '__main__':
    t1 = threading.Thread(target=worker)
    t1.start()
    time.sleep(4.9)
    print('Done')
