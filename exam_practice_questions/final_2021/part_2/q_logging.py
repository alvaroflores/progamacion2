from typing import Callable

from collections import OrderedDict
import time
import logging

# Empty basic config turns off default console handler

FORMAT = '%(asctime)s [%(levelname)s] %(funcName)s: %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class TimeBoundedLRU:

    def __init__(self, func: Callable, maxsize: int = 128, maxage: int = 30):
        self.cache = OrderedDict()  # { args : (timestamp, result)}
        self.func = func
        self.maxsize = maxsize
        self.maxage = maxage

    def __call__(self, *args, **kwargs):
        # function = keyword['func']
        function = kwargs.get('func', self.func)
        key = (args, function)
        if key in self.cache:
            self.cache.move_to_end(key)
            timestamp, result = self.cache[key]
            if time.time() - timestamp <= self.maxage:
                return result
        try:
            result = function(*args)
        except TypeError:
            logger.error('Bad Type')
        else:
            self.cache[key] = time.time(), result
            if len(self.cache) > self.maxsize:
                self.cache.popitem(False)
            return result


def query(x: int, y: int) -> int:
    print('Computing query 1...')
    time.sleep(2)
    if not (isinstance(x, int) and isinstance(y, int)):
        raise TypeError
    else:
        return x + y

def query_mul(x: int, y: int) -> int:
    print('Computing query 2...')
    time.sleep(2)
    if not (isinstance(x, int) and isinstance(y, int)):
        raise TypeError
    else:
        return x * y

tb_query = TimeBoundedLRU(query, maxage=1)
print(tb_query(1, 3, func=query))
time.sleep(1.1)
print(tb_query(1, 3, func=query))
print(tb_query(1, 3, func=query_mul))
print(tb_query(1, 3, func=query_mul))
# print(tb_query('a', 'b'))
