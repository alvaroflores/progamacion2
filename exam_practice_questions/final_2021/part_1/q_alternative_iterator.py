from collections import deque


class Alternative:
    def __init__(self, values: deque[int]):
        self.values = values
        self.right = True

    def __iter__(self):
        return self

    def __next__(self) -> int:
        if not self.values:
            raise StopIteration
        if self.right:
            self.right = False
            return self.values.pop()
        else:
            self.right = True
            return self.values.popleft()


for i in Alternative(deque([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])):
    print(i, end=' ')
