# Dictionary Merge & Update Operator

x = {"key1": "value1 from x", "key2": "value2 from x"}
y = {"key2": "value2 from y", "key3": "value3 from y"}
print(x | y)

print(y | x)


# Type Hinting Generics in Standard Collections

def greet_all(names: list[str]) -> None:
    for name in names:
        print("Hello", name)


# Zoneinfo

"""The zoneinfo module brings support for the IANA time zone database to the standard library. It 
adds zoneinfo.ZoneInfo, a concrete datetime.tzinfo implementation backed by the system’s time 
zone data. """

from zoneinfo import ZoneInfo
from datetime import datetime, timedelta

# Daylight saving time
dt = datetime(2020, 10, 31, 12, tzinfo=ZoneInfo("America/Los_Angeles"))
print(dt)

dt.tzname()

# Standard time
dt += timedelta(days=7)
print(dt)

print(dt.tzname())
