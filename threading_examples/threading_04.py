import os
import time
import threading

start = time.perf_counter()


def do_something(seconds):
    print(f'Sleeping {seconds} second(s)... parent process: {os.getppid()}  process id: {os.getpid()}')
    time.sleep(seconds)
    print(f'Done Sleeping...{seconds}')


if __name__ == '__main__':
    threads = []
    for _ in range(10):
        p = threading.Thread(target=do_something, args=(1,))
        p.start()
        threads.append(p)

    for process in threads:
        process.join()

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start, 2)} second(s)')

# Modified from https://www.youtube.com/watch?v=IEEhzQoKtQU
# https://github.com/rvlucerga/code_snippets/blob/master/Python/Threading/threading-demo.py