from random import randint
from threading import Thread, local, currentThread
from time import sleep


def show_value(data):
    try:
        val = data.value
    except AttributeError:
        print(currentThread().name, ' - No Value Yet')
    else:
        print(currentThread().name, 'value = ', val)


def worker(data):
    show_value(data)
    data.value = randint(1,100)
    show_value(data)


print(currentThread().name, ' - Starting')
local_data = local()
show_value(local_data)

for i in range(2):
    t = Thread(name="W"+str(i), target=worker, args=[local_data])
    t.start()
    sleep(0.3)
show_value(local_data)
print(currentThread().name, ' - Done')






