# https://anandology.com/python-practice-book/iterators.html

import os
import re

os.chdir("/Users/rlucerga/Dropbox (Personal)/Innitium/Clients/UFV/PII/06 Documents/hunt/"
         "prog_2/generator_files")

def readfiles(filenames):
    for f in filenames:
        for line in open(f):
            yield line

def grep(pattern, lines):
    return (line for line in lines if pattern in line)

def printlines(lines):
    for line in lines:
        print(line, end="")

def main():
    file_pattern = r".*\.txt"
    file_names = filter(lambda x: bool(re.match(file_pattern, x)), os.listdir())
    lines = readfiles(file_names)
    pattern = r"test"
    lines = grep(pattern, lines)
    printlines(lines)


if __name__ == '__main__':
    main()

