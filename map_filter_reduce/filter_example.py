from dataclasses import dataclass

@dataclass
class Person:
    name: str
    age: int


data = [Person('John', 54), Person('Phoebe', 21), Person('Adam', 19)]

d1 = filter(lambda p: p.age <= 21, data)
d2 = map(lambda p: p.name, d1)
print(list(d2))

from functools import reduce

data = [3, 4, -6, 2, -1, 9, 0, 7, -5, 8]

print(reduce(lambda x, y: x + (y * 0.21), data, 0))


cashflows = [1000, -90, -90, -90, -90]
reduce(lambda bal, pmt: bal*1.05 + pmt, cashflows)

from collections import Counter

Counter([i % 3 for i in range(101)])[0]

len(list(filter(lambda n: n % 3 == 0,range(101))))


[i for i in range(101) if i % 3 ==0]


from collections import defaultdict

e = defaultdict(lambda: 'no_grade')
e.update({'john': 'A', 'mary': 'A+'})

print(e['john'])
print(e['peter'])